from __future__ import division

import random

class TrashBag():

    """
    Trash bag class for the simulator.

    :param bag_volume: The bag volume.
    :param bag_min_weight: Minimum bag weight.
    :param bag_max_weight: Maximum bag weight.
    """

    def __init__(self, bag_volume, bag_min_weight, bag_max_weight):
        self.bag_volume = bag_volume
        self.bag_weight = random.uniform(bag_min_weight, bag_max_weight)

class Bin():

    """
    Bin class for the simulator.

    :param max_volume: Max volume that a bin can carry.
    :param threshold: Capacity threshold.
    :param bin_id: Bin id - it is the <areaIdx>.position. Position can't be 0.
    """

    def __init__(self, max_volume ,threshold, bin_id):

        """
        Initially all the bins are empty.
        """

        self.bin_id = bin_id
        self.max_volume = max_volume
        self.current_volume = 0
        self.current_weight = 0
        self.occupancy = 0
        self.exceeded = False
        self.overflowed = False
        self.threshold = threshold


    def update_occupancy(self):
        self.occupancy=self.current_volume/self.max_volume

    def put_trash_in(self,trash):

        self.current_volume += trash.bag_volume
        self.current_weight += trash.bag_weight
        self.update_occupancy()
        self.check_overflowed()
        self.check_exceeded()

    def check_exceeded(self):
        if self.threshold < self.occupancy:
            self.exceeded = True
        else:
            self.exceeded = False

    def check_overflowed(self):
        if round(self.current_volume, 3) > round(self.max_volume, 3):
            self.overflowed = True
        else:
            self.overflowed = False


class Lorry():

    """
    Lorry class for the simulator.

    :param max_volume: Maximum volume capacity for the lorry.
    :param max_weight: Maximum weight capacity for the lorry.
    :param area_index: The index of the area where the lorry resides.
    """
    def __init__(self,max_volume, max_weight, area_index):

        """
        Initially all the lorries are empty.
        """

        self.max_volume = max_volume
        self.max_weight = max_weight
        self.current_volume = 0
        self.current_weight = 0
        self.location = str(area_index)+'.0'    # initially all the lorries are the depot - so the location is area_index.0
        self.exceeded = False

    def get_trash(self, bin):

        """
        Get trash from a bin.
        """

        self.current_weight += bin.current_weight
        self.current_volume += bin.current_volume/2
        bin.current_weight = 0
        bin.current_volume = 0
        bin.occupancy = 0
        bin.exceeded = False
        bin.overflowed = False
        self.check_exceeded()

    def check_can_take_trash(self, bin):

        """
        Check if you can take the trash from the bin.
        This happens by updating the lorry contents, checking
        occupancy levels, reverting back to the previous lorry contents
        and reporting the result(True or False).
        """

        self.current_weight += bin.current_weight
        self.current_volume += bin.current_volume/2
        self.check_exceeded()
        if self.exceeded:
            self.current_weight -= bin.current_weight
            self.current_volume -= bin.current_volume / 2
            self.check_exceeded()
            return False
        else:
            self.current_weight -= bin.current_weight
            self.current_volume -= bin.current_volume / 2
            return True

    def empty_lorry(self):
        self.current_volume = 0
        self.current_weight = 0
        self.location = 0
        self.exceeded = False

    def check_exceeded(self):
        if self.current_volume > self.max_volume or self.current_weight > self.max_weight:
            self.exceeded = True


