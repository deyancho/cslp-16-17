from __future__ import division

import numpy as np

def isWhole(x):
    if float(x) % 1 == 0:
        return int(x)
    else:
        raise Exception

def scan_input(file):

    """
    Scans the input file for the word "experiment".

    :param file: path to the input file
    :return: index of the word experiment or -1 if not found
    """

    with open(file, 'r') as reader:
        data = reader.read()
    found = data.find('experiment')
    return found

def filter_input(file):

    """
    Returns a list of lists of strings that are the lines of the input file without the blanks and the commented lines.

    :param file: path to the input
    :return: list of strings
    """

    with open(file) as reader:
        lines = reader.readlines()

    file_list = []
    for line in lines:
        line = line.strip()
        if line.startswith('#') or line == '':
            continue
        else:
            file_list.append(line.split())

    return file_list


def complex_input_iterator(lst):

    """
    Need this function to make the iteration of the above returned_lst easier.
    """

    for val in lst:
        yield val


def row_check(row, num_bins, zero_index, area):

    """
    Checks a row of the road matrix for any mistakes.

    :param row: The row itself - it is of the form e.g. ['0', '3', '-1', '-1', '-1', '4'].

    :param num_bins: num_bins - how many bins there are in the area whose matrix we're looking at currently. Can help us detect if there's a correct number of entries in the current row.
    :param zero_index: zero_index - this is the index of the row we're looking at currently. This is also the index of the entry of this row which must be 0.
    :param area: This is the index of the area we're looking at currently - not the areaIdx, but the number with which the area appears in the input, starting from 0.
    :return row: return the row as a list of integers
    """

    if len(row) != num_bins + 1:
        raise Exception(
            "Error! The dimensions of the matrix for area {} don't match the number of bins in the area. First problematic row number: {}. The invalid tokenized row itself: {}. Terminating program! ".format(area,zero_index,row))
    if row[zero_index] != '0':
        raise Exception(
            "Error! Road matrix for area {} is not 0-diagonal! The invalid row itself: {}. Terminating program!".format(area,row))
    for id, elem in enumerate(row):
        if id == zero_index:
            row[id] = 0
        else:
            try:
                row[id] = isWhole(row[id])
            except:
                raise Exception(
                    "Error! Value for element [{}][{}] in the matrix of area {} is not an integer! Terminating program!".format(zero_index,id,area))
            if row[id] < -1 or row[id] > 255:
                raise Exception("Error! Value of element [{}][{}] in the matrix of area {} is not between -1 and 255(inclusive)! Terminating program! ".format(id,area,zero_index))
            elif row[id] == 0 and id != zero_index:
                raise Exception(
                    "Error! A non-diagonal element [{}][{}] in the matrix of area {} is 0! Please denote nodes with no path between them with -1, not with 0. Terminating program!".format(zero_index,id,area))
    return row


def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.
    Function from stackoverflow - Aaron Hall'''
    z = x[:]
    z.update(y)
    return z


def area_description_check_experimental(description, serviceFreq, area):

    """serviceFreq == 'no value' means that there isn't an experiment directive for serviceFreq
    if serviceFreq != 'no value', area descriptions of the format areaIdx 0 thresholdVal 0.75 noBins 5 are allowed (without the serviceFreq parameter)"""

    if len(description) != 8 and len(description) != 6 and not (len(description) == 6 and serviceFreq == 'no value'):
        raise Exception("Error! Invalid format for the description of area number {}. This invalid tokenised line is in the place of the area description: {}. Terminating program!".format(area,description))

    if len(description) == 6:
        if description[0] != 'areaIdx':
            raise Exception(
                "Error! Invalid name for areaIdx for area number {}. The invalid tokenized line is: {}. Terminating program!".format(area,description))
        try:
            int_area_idx = description[1]
        except:
            raise Exception("Error! Value for areaIdx of area number " + str(
                    area) + " is not an integer! The invalid tokenized line is : " + str(
                    description) + "Terminating program!")
        if int_area_idx != area:
            raise Exception("Error! The area indices must be valid array subscripts. I.e. - they must be 0, 1, 2 etc. in this order. (As answered in Piazza.) ")
        if int_area_idx < 0 or int_area_idx > 255:
            raise Exception(
                "Error! The value for areaIdx of area number " + str(
                    area) + " is not in the unsigned 8-bit integer range! The invalid tokenized line is: " + str(
                    description) + "Terminating program!")

        if description[2] != 'thresholdVal':
            raise Exception(
                "Error! Invalid name for thresholdVal of area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + "Terminating program!")
        try:
            threshold_val = float(description[3])
        except:
            raise Exception(
                "Error! Value for thresholdVal of area number " + str(
                    area) + " is not a float! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")
        if description[4] != 'noBins':
            raise Exception(
                "Error! Invalid name for noBins of area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + ". Terminating program!")
        try:
            no_bins = isWhole(description[5])
        except:

            raise Exception(
                "Error! Value for noBins of area number " + str(
                    area) + " is not an integer! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")
        if no_bins < 0 or no_bins > 65535:
            raise Exception(
                "Error! Value for noBins of area number " + str(
                    area) + " is not in the unsigned 16-bit integer range! The invalid tokenized line is: " + str(
                    description) + ". Terminating program")

        return {'areaIdx': int_area_idx, 'serviceFreq': serviceFreq, 'thresholdVal': threshold_val,
                'noBins': no_bins}

    else:
        if description[0] != 'areaIdx':
            raise Exception(
                "Error! Invalid name for areaIdx for area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + ". Terminating program!")
        try:
            int_area_idx = isWhole(description[1])
        except:

            raise Exception(
                "Error! Value for areaIdx of area number " + str(
                    area) + " is not an integer! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")
        if int_area_idx != area:
            raise Exception("Error! The area indices must be valid array subscripts. I.e. - areas must have area indices 0, 1, 2, 3 etc. areaIdx value should be " + str(area) + ". The invalid tokenized line is: " + str(description))
        if int_area_idx < 0 or int_area_idx > 255:
            raise Exception(
                "Error! Value for areaIdx of area number " + str(
                    area) + " is not in the unsigned 8-bit integer range! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")

        if description[2] != 'serviceFreq':
            raise Exception(
                "Error! Invalid name for serviceFreq for area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + ". Terminating program!")
        try:
            service_freq = float(description[3])
        except:
            raise Exception(
                "Error! Value for serviceFreq of area number " + str(
                    area) + " is not a float(even if experimentation values are valid please replace it with a valid value)! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")

        if description[4] != 'thresholdVal':
            raise Exception(
                "Error! Invalid name for thresholdVal for area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + ". Terminating program!")
        try:
            threshold_val = float(description[5])
        except:

            raise Exception(
                "Error! Value for thresholdVal of area number " + str(
                    area) + " is not a float! The invalid tokenized line is: " + str(
                    description) + ". Terminating program!")

        if description[6] != 'noBins':
            raise Exception(
                "Error! Invalid name for noBins for area number " + str(
                    area) + "! The invalid tokenized line is: " + str(description) + ". Terminating program!")
        try:
            no_bins = isWhole(description[7])
        except:
            raise Exception("Error! Value for noBins is not an integer! The invalid tokenized line is: " + str(
                description) + ". Terminating program!")
        if no_bins < 0 or no_bins > 65535:
            raise Exception(
                "Error! Value for noBins of area number " + str(
                    area) + " is not in the unsigned 16-bit integer range! The invalid tokenized line is: " + str(
                    description) + ". Terminating program")
        if serviceFreq != 'no value':
            return {'areaIdx': int_area_idx, 'serviceFreq': serviceFreq, 'thresholdVal': threshold_val,
                    'noBins': no_bins}
        else:
            return {'areaIdx': int_area_idx, 'serviceFreq': [service_freq], 'thresholdVal': threshold_val,
                    'noBins': no_bins}


def process_complex_input(lst_complex_input, serviceFreq):

    """
    :param lst_complex_input: a list of the form e.g. where the empty lists are already processed lines [[], [], [], [], [], [], [], [], [], ['noAreas', '1'], ['areaIdx', '0', 'serviceFreq', '0.0625', 'thresholdVal', '0.75', 'noBins', '5'], ['roadsLayout'], ['0',  '3', '-1', '-1', '-1', '4'], ['3', '0', '5', '-1', '-1', '-1'], ['-1', '-1',  '0', '2', '-1', '-1'], ['-1', '-1', '-1', '0',  '2',  '2'], ['-1','1', '-1', '-1', '0', '-1'], ['4', '-1', '-1','2',  '4', '0'], [], [], []]
    :param serviceFreq: a parameter which checks if a serviceFreq has been set as an experimentation directive
    :return: a list of dictionaries with the details of different areas
    """

    iterator = complex_input_iterator(lst_complex_input)
    list_of_area_dicts = []
    for val in iterator:
        if val == []:
            continue
        elif val[0] == 'noAreas':

            if len(val) != 2:
                raise Exception("Error! Invalid format for noAreas! The invalid tokenized line is: " + str(
                    val) + ". Terminating program!")
            try:
                int_no_areas = isWhole(val[1])
            except:

                raise Exception(
                    "Error! Value for volume of noAreas not an integer! The invalid tokenized line is: " + str(
                        val) + ". Terminating program!")
            if int_no_areas <= 0 or int_no_areas > 255:
                raise Exception(
                    "Error! Value for noAreas is not a positive unsigned 8-bit integer! The invalid tokenized line is: " + str(
                        val) + ". Terminating program!")

            num_areas = int(val[1])

            area = 0
            while num_areas > 0:
                try:
                    area_description = iterator.next()
                except:

                    raise Exception("Error! There's no area description for area " + str(
                        area) + "! noAreas value may be too large. Terminating program!")

                if area_description == []:
                    raise Exception(
                        "Error! There's a model parameter where there should be an area description for area " + str(
                            area) + "! noAreas may be too large or you may have just placed a model parameter in the wrong place. Terminating program!")

                description_dict = area_description_check_experimental(area_description, serviceFreq, area)
                num_bins = int(area_description[7])
                num_bins_const = num_bins
                index_zero = 0

                try:
                    road_layout = iterator.next()
                except:
                    raise Exception("Error! There's no roadsLayout string for area " + str(
                        area) + "! noAreas value may be too large. Terminating program!")
                if road_layout == []:

                    raise Exception(
                        "Error! There's a model parameter where there should be a roadsLayout string for area " + str(
                            area) + "! noAreas may be too large or you may have just placed a model parameter in the wrong place. Terminating program!")
                elif road_layout == ['roadsLayout']:
                    pass
                else:
                    raise Exception("Error! " + "There is/are token/s " + str(
                        road_layout) + " where there should be a ['roadsLayout'] token! Terminating program!")
                layout_matrix = []
                while num_bins + 1 > 0:
                    try:
                        row = iterator.next()
                    except:

                        raise Exception("Error! There's a missing row of a matrix for area " + str(
                            area) + "! noAreas value may be too large or there may not be enough rows in the matrix. Terminating program!")
                    if row == []:
                        raise Exception(
                            "Error! There's a model parameter where there should be a row of a matrix for area " + str(
                                area) + "! noAreas may be too large or the matrix for the area doesn't have enough rows(number of rows less than bin number+1) or you may have just placed a model parameter in the wrong place. Terminating program!")
                    row_processed = row_check(row, num_bins_const, index_zero, area)
                    index_zero += 1
                    num_bins -= 1
                    layout_matrix.append(row_processed)
                area += 1
                description_dict['roadsLayout'] = np.array(layout_matrix)
                list_of_area_dicts.append(description_dict)
                num_areas -= 1
            continue
        else:
            raise Exception("Error! There's an invalid line in the input file. The tokenized line is: " + str(
                val) + ". Terminating program!")

    return int_no_areas, list_of_area_dicts


def sanity_check_experimental(model_simple_att, dict_of_areas):
    area_indices = []
    serviceFreqExperimented = type(dict_of_areas[0]['serviceFreq']) > 1
    bin_service_rate = 3600 / model_simple_att['binServiceTime']
    if model_simple_att['binVolume'] > model_simple_att['lorryVolume']:
        raise Exception("Error! Bin volume exceeds lorry volume! Terminating program!")
    elif model_simple_att['bagVolume'] > model_simple_att['binVolume']:

        raise Exception('Error! Bag volume exceeds bin volume! Terminating program!')
    elif model_simple_att['bagWeightMax'] > model_simple_att['lorryMaxLoad']:

        raise Exception("Error! Maximum bag weight exceeds lorry weight capacity! Terminating program!")
    elif model_simple_att['bagWeightMin'] > model_simple_att['bagWeightMax']:

        raise Exception("Error! Bag minimum weight exceeds bag maximum weight! Terminating program!")
    elif model_simple_att['warmUpTime'] > model_simple_att['stopTime']:
        print('Warning! Warm up time exceeds stop time! You will not be able to perform experiments or output statistics. Only detailed output available.')

    if serviceFreqExperimented:

        for rate in model_simple_att['disposalDistrRate']:

            for shape in model_simple_att['disposalDistrShape']:
                for frequency in dict_of_areas[0]['serviceFreq']:
                    avg_disposals_per_hour = shape / rate
                    avg_disposals_per_hour_str = str(avg_disposals_per_hour)
                    if frequency > avg_disposals_per_hour:
                        print("Warning! Schedule frequency per hour " + "(" + str(
                            frequency) + ") " + " exceeds average thrash disposals per hour (k/lambda) - " + str(
                            avg_disposals_per_hour_str) + " for a bin. Troubling parameters: disposalDistrRate - " + str(
                            rate) + ",disposalDistrShape - " + str(shape) + ", serviceFreq - " + str(frequency))

        for freq in dict_of_areas[0]['serviceFreq']:
            if bin_service_rate < freq:
                print(
                    "Warning! A schedule interval takes less time than emptying a bin! (troubling parameters: " + "binServiceTime - " + str(
                        model_simple_att['binServiceTime']) + ", serviceFreq experimental - " + str(freq)+ ")")

    else:
        for rate in model_simple_att[
            'disposalDistrRate']:  # model_simple_att['disposalDistrRate'] - list of experiment values for disposalDistrRate

            for shape in model_simple_att['disposalDistrShape']:
                for area in dict_of_areas:

                    avg_disposals_per_hour = shape / rate
                    if area['serviceFreq'][0] > avg_disposals_per_hour:
                        area_idx = str(area['areaIdx'])
                        service = str(area['serviceFreq'][0])
                        print(
                            "Warning! Schedule frequency per hour " + "(" + service + ") " + "for area with areaIdx " + area_idx + " exceeds average thrash disposals per hour (k/lambda) - " + str(
                                avg_disposals_per_hour) + " for a bin.")
                    if bin_service_rate < area['serviceFreq'][0]:
                        print(
                            "Warning! A schedule interval takes less time than emptying a bin for area with areaIdx " + str(
                                area_idx) + "!(troubling parameters: binServiceTime - " + str(
                                model_simple_att['binServiceTime']) + ", serviceFreq - " + str(service))
                    for idx, row in enumerate(area['roadsLayout']):
                        for id, col in enumerate(row):
                            if col > 0:
                                walk_per_hour = 60 / col
                                if walk_per_hour < area['serviceFreq'][0]:
                                    print("Warning! Road matrix for area with areaIdx " + str(area['areaIdx']) + " has a road which takes more time to drive through than the length of the schedule interval! " + "(index of road" + "[" + str(
                                        idx) + "]" + "[" + str(
                                        id) + "]" + ") (troubling parameters: roadsLayout, serviceFreq)")

    for area in dict_of_areas:
        area_indices.append(area['areaIdx'])



def verify_input(file):

    """
    Verifies inputs and returns the model parameters.

    :param file: path to the input file
    :return: model_simple_attributes - dictionary of simple attributes and their values. The keys are the string attributes lorryVolume, lorryMaxLoad, binServiceTime, binVolume, disposalDistrRate, disposalDistrShape, bagVolume, bagWeightMin, bagWeightMax, stopTime and warmUpTime, so the dictionary is {'lorryVolume': <value>, 'lorryMaxLoad': <value>, ... 'stopTime': <value>}
    :return: dict_of_areas - list of dicts holding data about the areas
    """

    # 1st run

    filtered_lst_file = filter_input(file)

    elems_to_check = ['lorryVolume', 'lorryMaxLoad', 'binServiceTime', 'binVolume', 'disposalDistrRate',
                      'disposalDistrShape', 'bagVolume', 'bagWeightMin', 'bagWeightMax',
                      'stopTime', 'warmUpTime']

    # 2nd run
    lst_complex_input, model_simple_attributes, serviceFreq = check_simple_attributes_experimental(elems_to_check, filtered_lst_file)
    number_areas, dict_of_areas = process_complex_input(lst_complex_input, serviceFreq)
    model_simple_attributes['noAreas'] = number_areas

    sanity_check_experimental(model_simple_attributes, dict_of_areas)
    return model_simple_attributes, dict_of_areas


def check_simple_attributes_experimental(elemsToCheck, splitlist):

    """
    :param elemsToCheck:
    :param splitlist:
    :return:
    """

    elementsValidInput = elemsToCheck[:]
    model_values = {}
    returned_lst = splitlist[:]
    valuesExperimented = []
    serviceFreq = 'no value'

    for ind, lst in enumerate(splitlist):

        if lst[0] in elementsValidInput and lst[0] not in elemsToCheck and lst[1] != 'experiment':

            print("Warning! - you're trying to assign " + str(lst[0]) + " on line " + str(
                ind) + " (don't count the commented and the whitespace lines, start counting from 0) when it has already been assigned! Ignoring assignment!")
            returned_lst[ind] = []


        elif lst[0] in elemsToCheck and lst[0] in elementsValidInput and lst[0] not in ['disposalDistrRate',
                                                                                        'disposalDistrShape']:  # and elemsToCheck in elemsValInput
            if len(lst) > 2:
                print(
                "Warning! - {} entry may have more than 1 assignment on line {}(don't count white lines and commented lines, start counting from 0)! The validity of only the first token will be checked!".format(
                    lst[0], ind))
            if lst[0] in ['lorryVolume', 'lorryMaxLoad', 'binServiceTime']:
                try:
                    value = isWhole(lst[1])
                except:
                    raise Exception(
                        "Error! Value for {} is not an integer! The invalid tokenized line is: {}. Terminating program!".format(
                            lst[0], lst))
                if lst[0] == 'lorryVolume':
                    if value < 0 or value > 255:
                        raise Exception(
                            "Error! Value for {} is not an unsigned 8-bit range! The invalid tokenized line is: {}. Terminating program!".format(
                                lst[0], lst))
                elif lst[0] in ['lorryMaxLoad', 'binServiceTime']:
                    if value < 0 or value > 65535:
                        raise Exception(
                            "Error! Value for {} is not an unsigned 16-bit range! The invalid tokenized line is: {}. Terminating program!".format(
                                lst[0], lst))

            elif lst[0] in ['binVolume', 'bagVolume', 'bagWeightMin', 'bagWeightMax', 'stopTime', 'warmUpTime']:
                try:
                    value = float(lst[1])
                except:
                    raise Exception(
                        "Error! Value for {} is not a float! The invalid tokenized line is: {}. Terminating program!".format(
                            lst[0], lst))
                if value <= 0:
                    raise Exception(
                        "Error! Value for {} is not a positive number! The invalid tokenized line is: {}. Terminating program!".format(
                            lst[0], lst))

            elemsToCheck.remove(lst[0])
            returned_lst[ind] = []
            model_values[lst[0]] = value

        elif lst[0] in elemsToCheck and lst[0] in ['disposalDistrRate', 'disposalDistrShape']:
            if lst[0] == 'disposalDistrRate':
                if len(lst) > 2:
                    if lst[1] != 'experiment':
                        print("Warning! - disposalDistrRate entry may have more than 1 assignment on line " + str(
                            ind) + "(don't count white lines and commented lines, start counting from 0)! The validity of only the first token will be checked!")
                        try:
                            float_disposal_distr = float(lst[1])
                        except:
                            raise Exception(
                                "Error! Value for disposalDistrRate not a float! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        if float_disposal_distr <= 0:
                            raise Exception(
                                "Error! disposalDistrRate must be a positive number! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        elemsToCheck.remove(lst[0])
                        returned_lst[ind] = []
                        model_values['disposalDistrRate'] = [float_disposal_distr]

                    else:

                        exp_values_disposalDistrRate = []
                        for val_rate in lst[2:]:
                            try:
                                val_rate_float = float(val_rate)
                            except:

                                raise Exception(
                                    "Error! Experimentation value for disposalDistrRate is not a float! Invalid tokenized line is: " + str(
                                        lst) + ". Terminating program!")
                            if val_rate_float <= 0:
                                raise Exception(
                                    "Error! Experimentation value for disposalDistrRate must be a positive number! Invalid tokenized line is: " + str(
                                        lst) + ". Terminating program!")
                            exp_values_disposalDistrRate.append(val_rate_float)


                        elemsToCheck.remove(lst[0])
                        returned_lst[ind] = []
                        model_values['disposalDistrRate'] = exp_values_disposalDistrRate
                        valuesExperimented.append('disposalDistrRate')

                else:
                    try:
                        float_disposal_distr = float(lst[1])
                    except:

                        raise Exception(
                            "Error! Value for disposalDistrRate not a float! Invalid tokenized line is: " + str(
                                lst) + ". Terminating program!")
                    if float_disposal_distr <= 0:
                        raise Exception(
                            "Error! disposalDistrRate must be a positive number! Invalid tokenized line is: " + str(
                                lst) + ". Terminating program!")
                    elemsToCheck.remove(lst[0])
                    returned_lst[ind] = []
                    model_values['disposalDistrRate'] = [float_disposal_distr]

            elif lst[0] == 'disposalDistrShape':
                if len(lst) > 2:
                    if lst[1] != 'experiment':
                        print("Warning! - disposalDistrShape entry may have more than 1 assignment on line " + str(
                            ind) + "(don't count white lines and commented lines, start counting from 0)! The validity of only the first token will be checked!")
                        try:
                            int_distr_shape = isWhole(lst[1])
                        except:

                            raise Exception(
                                "Error! Value for volume of disposalDistrShape not an integer! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        if int_distr_shape < 0 or int_distr_shape > 255:
                            raise Exception(
                                "Error! Value for disposalDistrShape not in the unsigned 8-bit integer range! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        if int_distr_shape == 0:
                            raise Exception(
                                "Error! Value for disposalDistrShape can't be 0! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        elemsToCheck.remove(lst[0])
                        returned_lst[ind] = []
                        model_values['disposalDistrShape'] = [int_distr_shape]
                    else:
                        exp_values_disposalDistrShape = []
                        for val_shape in lst[2:]:
                            try:
                                val_shape_int = isWhole(val_shape)
                            except:

                                raise Exception(
                                    "Error! Experimentation value for disposalDistrShape not an int! Invalid tokenized line is: " + str(
                                        lst) + ". Terminating program!")
                            if val_shape_int < 0 or val_shape_int > 255:
                                raise Exception(
                                    "Error! Experimentation value for disposalDistrShape not in the unsigned 8-bit integer range! Invalid tokenized line is: " + str(
                                        lst) + ". Terminating program!")
                            if val_shape_int == 0:
                                raise Exception(
                                    "Error! Experimentation value for disposalDistrShape can't be 0! Invalid tokenized line is: " + str(
                                        lst) + ". Terminating program!")

                            exp_values_disposalDistrShape.append(val_shape_int)

                        if lst[0] in elemsToCheck:
                            elemsToCheck.remove(lst[0])
                        returned_lst[ind] = []
                        model_values['disposalDistrShape'] = exp_values_disposalDistrShape
                        valuesExperimented.append('disposalDistrShape')
                else:
                    try:
                        int_distr_shape = isWhole(lst[1])
                    except:

                        raise Exception(
                            "Error! Value for volume of disposalDistrShape not an integer! Invalid tokenized line is: " + str(
                                lst) + ". Terminating program!")
                    if int_distr_shape < 0 or int_distr_shape > 255:
                        raise Exception(
                            "Error! Value for disposalDistrShape not in the unsigned 8-bit integer range! Invalid tokenized line is: " + str(
                                lst) + ". Terminating program!")
                    if int_distr_shape == 0:
                        raise Exception("Error! Value for disposalDistrShape can't be 0! Invalid tokenized line is: " + str(
                            lst) + ". Terminating program!")
                    elemsToCheck.remove(lst[0])
                    returned_lst[ind] = []
                    model_values['disposalDistrShape'] = [int_distr_shape]

        elif lst[0] in elementsValidInput and lst[0] not in elemsToCheck and lst[0] in ['disposalDistrRate', 'disposalDistrShape']:
            if lst[0] == 'disposalDistrRate':

                if lst[0] in valuesExperimented:
                    print(
                        "Warning! You're trying to define experiment directive for disposalDistrRate but there already is one! Ignoring line.")
                    returned_lst[ind] = []
                else:
                    exp_values_disposalDistrRate = []
                    for val_rate in lst[2:]:
                        try:
                            val_rate_float = float(val_rate)
                        except:

                            raise Exception(
                                "Error! Experimentation value for disposalDistrRate is not a float! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        if val_rate_float <= 0:
                            raise Exception(
                                "Error! Experimentation value for disposalDistrRate must be a positive number! Invalid tokenized line is: " + str(
                                    lst) + ". Terminating program!")
                        exp_values_disposalDistrRate.append(val_rate_float)

                    returned_lst[ind] = []
                    model_values['disposalDistrRate'] = exp_values_disposalDistrRate
                    valuesExperimented.append('disposalDistrRate')

            elif lst[0] == 'disposalDistrShape':
                if len(lst) > 2:
                    if lst[0] in valuesExperimented:
                        print("Warning! You're trying to define experiment directive for disposalDistrShape but there already is one! Ignoring line.")
                    else:
                        exp_values_disposalDistrShape = []
                        for val_shape in lst[2:]:
                            try:
                                val_shape_int = isWhole(val_shape)
                            except:
                                raise Exception(
                                    "Error! Experimentation value for disposalDistrShape not an int! Invalid tokenized line is: {}. Terminating program!".format(lst))
                            if val_shape_int < 0 or val_shape_int > 255:
                                raise Exception(
                                    "Error! Experimentation value for disposalDistrShape not in the unsigned 8-bit integer range! Invalid tokenized line is: {}. Terminating program!".format(lst))
                            if val_shape_int == 0:
                                raise Exception("Error! Experimentation value for disposalDistrShape can't be 0! Invalid tokenized line is: {}. Terminating program!".format(lst))
                            exp_values_disposalDistrShape.append(val_shape_int)

                        model_values['disposalDistrShape'] = exp_values_disposalDistrShape
                        valuesExperimented.append('disposalDistrShape')
                    returned_lst[ind] = []

        elif lst[0] == 'serviceFreq' and lst[1] == 'experiment':
            if 'serviceFreq' in valuesExperimented:
                print("Warning! You're trying to define experiment directive for serviceFreq but there already is one! Ignoring line.")
                returned_lst[ind] = []
            else:
                serviceFreq = []
                for servFreq_val in lst[2:]:
                    try:
                        servFreq_val_float = float(servFreq_val)
                    except:
                        raise Exception("Error! Experimentation value for serviceFreq not a float! Invalid tokenized line is: {}. Terminating program!".format(lst))
                    if servFreq_val_float <= 0:
                        raise Exception("Error! Experimentation value for serviceFreq must be a positive number! Invalid tokenized line is: {}. Terminating program!".format(lst))
                    serviceFreq.append(servFreq_val_float)
                returned_lst[ind] = []
                valuesExperimented.append('serviceFreq')

    if elemsToCheck != []:
        for el in elemsToCheck:
            print("Missing value for " + str(el) + "!")
        raise Exception(
            "Error! Missing parameters in the input file or some/all parameters contain a spelling error! Terminating program!")
    return returned_lst, model_values, serviceFreq
