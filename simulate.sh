#!/bin/bash
# This script will be used to launch simulations 


# TODO - add functionality for adding some graphs maybe

numargs=$#

if [[ $numargs == 0 ]] ; then
  echo "Usage options:"
  echo "1) ./simulate.sh INPUT experiment (to run non-experimental input without detailed output)"
  echo "2) ./simulate.sh INPUT (to run non-experimental input with detailed output)"
  echo "3) ./simulate INPUT (to run experimental input without detailed output)"
  echo "4) If you run an experimental input with the option experiment you'll receive a message that tells you that experimental inputs don't show detailed output by specification and you should remove that option."
  exit 0
fi

if [[ -e $1 ]] ; then
  python simulation.py "$@"
else
  echo "Error! The file $1 does not exist. Terminating program."
fi


