from datetime import datetime, timedelta
from numpy.random import uniform

"""
File for testing output formatting for time
"""

start_time = datetime(year=2016, month=4, day = 1, hour = 0, minute = 0, second = 0 ) # don't mind the year, month and day values - we use it like that because
end_time = 36
hours = 0
iteration = 0
while (start_time.day -1)* 24 + start_time.hour <  end_time:

    rand = uniform(0,10)
    start_time += timedelta(seconds = rand)
    if (start_time.day -1)* 24 + start_time.hour >=  end_time:
        break
    if start_time.day <= 10:
        str_to_print = "0" + str(start_time.day - 1) + ":" + start_time.strftime("%H:%M:%S")
    else:
        str_to_print = str(start_time.day - 1) + start_time.strftime("%H:%M:%S")
    print(str_to_print)
