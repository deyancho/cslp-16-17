from __future__ import division

import sys
from sim_components import TrashBag, Bin, Lorry
from input_verification import scan_input, verify_input
from datetime import datetime, timedelta
import numpy as np
import networkx as nx
from Queue import PriorityQueue
import copy


def main():

    """runs the whole program"""

    input_file = sys.argv[1]
    experimental = scan_input(input_file)

    try:
        if len(sys.argv) > 2:
            raise Exception("Error! No options implemented.")
        else:
            model_of_attributes, description_of_areas = verify_input(input_file)
            if experimental == -1:
                algorithm(model_of_attributes, description_of_areas,0)  # if the file doesn't contain the word experiment run algorithm() with detailed output.
            else:
                algorithm_exp(model_of_attributes, description_of_areas)   # else run algorithm_exp()
    except Exception as e:
        print(e)
        sys.exit(1)


def to_graph(area, model):

    """
	Return a graph from an area dictionary and the model of parameters

	:param area: dictionary containing the area description
	:param model: the parameters of the model
	:return: Graph of the area, filled up with bins for each node
	"""

    np_ndarray = area['roadsLayout']

    for index, row in enumerate(np_ndarray):
        for id, col in enumerate(row):
            if col == -1:
                np_ndarray[index][id] = 0
    np_matrix = np.matrix(np_ndarray)

    # We change -1 to 0 and made the ndarray to be a numpy matrix, because nx.from_numpy_matrix() requires it.

    G = nx.from_numpy_matrix(np_matrix, create_using=nx.MultiDiGraph())

    # Fill up every node except the 1st with a data piece - specifically - a Bin()

    for index, node in enumerate(G.nodes(data=True)[1:], start=1):
        node[1]['bin'] = Bin(model['binVolume'], area['thresholdVal'], index)

    # Fill up the first 1st node with a data piece - specifically - a Lorry()
    G.nodes(data=True)[0][1]['lorry'] = Lorry(model['lorryVolume'], model['lorryMaxLoad'],area['areaIdx'])

    return G

def algorithm(model, areas, show_stats_only):

    """
	Runs the algorithm for non-experimental input files.

	:param model: dictionary of the form for example {'bagWeightMin': 2.0, 'warmUpTime': 12.0, 'noAreas': 1, 'lorryMaxLoad': 7000, 'bagVolume': 0.6, 'bagWeightMax': 8.0, 'binVolume': 2.0, 'lorryVolume': 20, 'disposalDistrRate': 2.0, 'disposalDistrShape': 2, 'binServiceTime': 130.0, 'stopTime': 42.0}

	:param areas: list of dictionaries containing the area descriptions and road layouts for example (this list has only 1 dictionary i.e. only 1 area) [{'thresholdVal': 0.75, 'serviceFreq': [0.0625], 'noBins': 5, 'areaIdx': 0, 'roadsLayout': array([[ 0,  3, -1, -1, -1,  4],
	   [ 3,  0,  5, -1, -1, -1],
	   [-1, -1,  0,  2, -1, -1],
	   [-1, -1, -1,  0,  2,  2],
	   [-1,  1, -1, -1,  0, -1],
	   [ 4, -1, -1,  2,  4,  0]])}]

	:param show_stats_only: 1 if yes, 0 if no

	:return: None
	"""

    bins_areas = []
    for idx, area in enumerate(areas):

        bins_areas.append(area)

        bins_areas[idx]['roadsLayout'] = to_graph(area, model)

    # bins_areas becomes the same thing as the areas argument but the roadsLayout is a networkX multigraph.

    # start time - it is easier to work with timedelta - however, python doesn't allow for year, month and day to be 0, but we can still use the class like this.
    time = datetime(year=2016, month=4, day=1, hour=0, minute=0,
                          second=0)

    # end time
    max_time = time + timedelta(hours=model['stopTime'])

    # queue for bin filling events
    bin_fill_queue = PriorityQueue()

    # schedule times  - the ith index is the time of the schedule for the ith area
    schedules = []

    # lorry references - the ith index is the lorry of the ith area

    lorries = []
    for area in bins_areas:
        lorries.append(lorry_return(area['roadsLayout']))

    # Initial setup below

    for area in bins_areas:

        # create the first schedule times

        time_schedule = time + timedelta(hours=1/area['serviceFreq'][0])
        schedules.append(time_schedule)

        areaIdx = area['areaIdx']

        # Put 1 trash bag in all of the bins

        for bin_id, bin in enumerate(area['roadsLayout'].nodes(data=True)[1:], start=1):

            bin = bin[1]['bin']
            delay = 0

            # Calculate the delay
            for i in range(0, model['disposalDistrShape'][0]):
                delay += -(1 / model['disposalDistrRate'][0]) * np.log(np.random.random())

            bag = TrashBag(model['bagVolume'], model['bagWeightMin'], model['bagWeightMax'])
            time_future = time + timedelta(hours=delay)
            bin_fill_queue.put((time_future, [(area['areaIdx'], bin, 'dispose', bag)]))

    # The time when statistics start being collected
    statistics_time = time + timedelta(hours=model['warmUpTime'])

    # Structures for statistics

    average_trip_duration = [[] for i in range(len(bins_areas))]    # List of empty lists initially. The i-th list contains the durations of all the trips in the i-th area.

    no_trips_areas = [0]*len(bins_areas)    # The number of trips in different areas - initially all 0's.

    average_trip_efficiency = [[] for i in range(len(bins_areas))]  # List of empty lists initially. The i-th list contains tuples of the form (time, trash) where time is duration of a specific trip and trash is the weight collected for that trip

    average_volume_collected = [[] for i in range(len(bins_areas))] # List of empty lists initially. The i-th list contains tuples of the form (time, volume) where time is duration of a specific trip and volume is the volume collected for that trip

    average_percentage_overflowed = [[] for i in range(len(bins_areas))]  # List of empty lists initially. The i-th list contains percentages of overflowed bins for the schedules of this area, e.g. [[0,16,0.17]] means 1 area and 2 schedules - in the beginning of the first 16% are overflowed,, in the beginning of the second 17%


    # Initial schedules are not missed
    missed_schedules = [False for i in range(len(bins_areas))]  # List of booleans - the ith bool indicates that the schedule for area i is initially not missed (all Falses in the start)

    # Initially all lorries are at the depot
    at_depots = [True for i in range(len(bins_areas))]  # List of booleans - the ith bool indicates that the lorry for area i is initially in the depot (all Trues in the start)

    # Queues for the actions of the lorries for the different areas. The queues may contain emptying a bin, leaving a location, arriving at a location, or empting the lorry.
    queues_routes = [PriorityQueue() for i in range(len(bins_areas))]

    # Bins to collect - initally none
    bins_to_collect = [{} for i in range(len(bins_areas))] # List of dicts for the priorities of the relevant areas. Initially no bins are set for collection.

    # Don't start new schedule until lorry has been emptied
    emptying_done = [True for i in range(len(bins_areas))]

    # The trip has ended initially for all the lorries in the areas.
    trip_ended = [True for i in range(len(bins_areas))]

    trip_start_time = [0]*len(bins_areas)    # This keeps track of the start times of the trips for the different areas. Initially it's [0,0,...] but it usually is [datetime, datetime,...]

    # Don't collect stats initially for all of the areas. (warmUpTime > 0)
    collect_stats = [False for i in range(len(bins_areas))]

    while not bin_fill_queue.empty() or not all_PQs_empty(queues_routes):   # while there are no lorry and bin events left

        # Choose the event from the bin filling queue and the lorry action queues with the smallest time value.

        # (inner workings - black box)
        events_to_choose_from_queue = PriorityQueue()
        events_to_choose_from_list = [[] for i in range(len(bins_areas)+1)]
        event_bin_fill = bin_fill_queue.get()
        events_to_choose_from_queue.put((event_bin_fill, 0))
        events_to_choose_from_list[0] = event_bin_fill

        for index, q in enumerate(queues_routes, start=1):
            if not q.empty():
                route_event = q.get()
                events_to_choose_from_queue.put((route_event, index))
                events_to_choose_from_list[index] = route_event

        # Get the event then return all the other events back to their respective queues.

        event_tuple = events_to_choose_from_queue.get()
        event = event_tuple[0]
        idx_to_remove = event_tuple[1]

        if idx_to_remove != 0 and events_to_choose_from_list[0] != []:
            bin_fill_queue.put(events_to_choose_from_list[0])

        for i, e in enumerate(events_to_choose_from_list[1:], start=1):
            if idx_to_remove != i and e != []:
                queues_routes[i-1].put(e)

        # (black box end)

        time = event[0]

        for i, s in enumerate(schedules):
            if time >= s:
                missed_schedules[i] = True
            if not missed_schedules[i]:
                pass
            else:
                if not at_depots[i]:
                    pass
                else:
                    if emptying_done[i]:    # if a lorry's at the depot, it has been emptied and it's time for a new schedule - plan the route

                        emptying_done[i] = False

                        bins_to_collect[i] = route_plan(time, queues_routes[i], bins_areas[i], model, True,
                                                        bins_to_collect[i])

                        schedules[i] += timedelta(hours=1 / bins_areas[i]['serviceFreq'][0])    # compute the next schedule time for that area.
                        missed_schedules[i] = False
                        if collect_stats[i]:
                            average_percentage_overflowed[i].insert(0, count_percentage_overflowed(bins_areas[i]))

                    else:   # This 'else' situation arises when the lorry is at depot and a schedule is missed, but the lorry hasn't been emptied which means that it's just passing through the depot.
                            # Even though it's passing through the depot, we don't take snapshots because we're still trying to achieve the previous service(we're in the previous schedule).
                            # It's arguable whether we should take snapshots in this case,
                            # it's not specified in Piazza or the handout.
                            # The handout implies that if the lorry is exceeding we should go back to the depot and empty it and if a schedule is missed, then reschedule - however, in this case the lorry is not exceeding,
                            # but it's just passing through.
                        pass

        # Time string formatting
        if time > max_time:
            break
        if time.day <= 10:
            str_to_print = "0" + str(time.day - 1) + ":" + time.strftime("%H:%M:%S")
        else:
            str_to_print = str(time.day - 1) + ":" + time.strftime("%H:%M:%S")

        event_description = event[1][0][2]

        # check the nature of the extracted event and act accordingly

        if event_description == 'leaving':
            if not show_stats_only:
                print(str_to_print + " -> " + "lorry " +  str(event[1][0][0]) + " leaving location " + str(event[1][0][0])+ "." + str(event[1][0][1]))
            if event[1][0][1] == 0:  # leaving the depot
                at_depots[event[1][0][0]] = False
                if trip_ended[event[1][0][0]]:   # if the last trip has ended - start a new one
                    trip_ended[event[1][0][0]] = False


                    if time > statistics_time:
                        collect_stats[event[1][0][0]] = True    # start collecting stats for area event[1][0][0] (here the area index)

                    if collect_stats[event[1][0][0]]:
                        trip_start_time[event[1][0][0]] = time
                        average_trip_efficiency[event[1][0][0]].insert(0,0)  # usuallly here it is something of the form [[(time, trash), (time,trash),(time, trash)],[0,(time, trash), (time,trash)], [225, (time, trash), (time,trash),(time, trash), (time,trash)]] # we have 0 in the second list and 225 in the third list, because this represents the trash that we're currently collecting for these areas.
                                                                             # for list 1 in the comment line above, there's no such number, because we're not in a trip for that area
                        average_volume_collected[event[1][0][0]].insert(0,0) # same thing for volumes

            del bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['lorry']  # delete the dictionary key for the lorry from the relevant node in the graph - the lorry's not there anymore

        elif event_description == 'arriving':
            if not show_stats_only:
                print(str_to_print + " -> " + "lorry " + str(event[1][0][0]) + " arriving at location " + str(
                    event[1][0][0]) + "." + str(event[1][0][1]))
            if event[1][0][1] == 0:
                at_depots[event[1][0][0]] = True
            bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['lorry'] = lorries[event[1][0][0]]    # add the dictionary key for the lorry to the relevant node in the graph

        elif event_description == 'emptyLorry':
            # my definition of a trip ending is that the lorry is emptied.
            lorries[event[1][0][0]].empty_lorry()
            if not show_stats_only:
                print(str_to_print + " -> " + "load of lorry " + str(event[1][0][0]) + " became " + str(round(lorries[event[1][0][0]].current_weight,3)) + " kg and contents volume " +  str(round(lorries[event[1][0][0]].current_volume,3)) +   " m^3")
            emptying_done[event[1][0][0]] = True    # This may prompt the lorry to start a new schedule(if it's time) or a new service(if it's not time for new schedule and you still have to collect trash for the current schedule).
            trip_ended[event[1][0][0]] = True

            if collect_stats[event[1][0][0]]:
                time_diff = time - trip_start_time[event[1][0][0]]
                no_trips_areas[event[1][0][0]] += 1     # a trip has ended once the lorry is emptied
                average_trip_duration[event[1][0][0]].append(time_diff)
                weight_collected=average_trip_efficiency[event[1][0][0]].pop(0)

                average_trip_efficiency[event[1][0][0]].insert(0,(time_diff, weight_collected))
                volume_collected = average_volume_collected[event[1][0][0]].pop(0)
                average_volume_collected[event[1][0][0]].insert(0, (time_diff, volume_collected))


        elif event_description == 'emptying':

            if not lorries[event[1][0][0]].check_can_take_trash(bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['bin']): # check if a lorry can take the trash at a location - if it can't go to depot
                queues_routes[event[1][0][0]] = PriorityQueue()
                return_to_depot(time, event[1][0][1], bins_areas[event[1][0][0]], queues_routes[event[1][0][0]], model, False, bins_to_collect[event[1][0][0]])
            else:
                if collect_stats[event[1][0][0]]:
                    average_trip_efficiency[event[1][0][0]][0] += bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['bin'].current_weight
                    average_volume_collected[event[1][0][0]][0] += bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['bin'].current_volume

                # if the lorry can take the trash remove it from the bins_to_collect dictionary and if the dictionary is empty, then return to the depot because you've emptied all the assigned bins

                lorries[event[1][0][0]].get_trash(
                    bins_areas[event[1][0][0]]['roadsLayout'].nodes(data=True)[event[1][0][1]][1]['bin'])
                if not show_stats_only:
                    print(str_to_print + " -> " + "load of bin " + str(event[1][0][0]) + "." + str(
                        event[1][0][1]) + " became 0 kg and contents volume 0 m^3")
                    print(str_to_print + " -> " + "load of lorry " + str(event[1][0][0]) + " became " + str(round(lorries[event[1][0][0]].current_weight,3)) + " kg and contents volume " +  str(round(lorries[event[1][0][0]].current_volume,3)) + " m^3")
                if event[1][0][1] in bins_to_collect[event[1][0][0]]['highestPriority']:
                    bins_to_collect[event[1][0][0]]['highestPriority'].remove(event[1][0][1])
                elif event[1][0][1] in bins_to_collect[event[1][0][0]]['mediumPriority']:
                    bins_to_collect[event[1][0][0]]['mediumPriority'].remove(event[1][0][1])
                elif event[1][0][1] in bins_to_collect[event[1][0][0]]['lowestPriority']:
                    bins_to_collect[event[1][0][0]]['lowestPriority'].remove(event[1][0][1])

                if bins_to_collect[event[1][0][0]]['highestPriority'] == [] and bins_to_collect[event[1][0][0]]['mediumPriority'] == [] and bins_to_collect[event[1][0][0]]['lowestPriority'] == []:
                    queues_routes[event[1][0][0]] = PriorityQueue()
                    return_to_depot(time, event[1][0][1], bins_areas[event[1][0][0]], queues_routes[event[1][0][0]],
                                    model, True, bins_to_collect)

        elif event_description == 'dispose':

            exceeded = event[1][0][1].exceeded
            overflowed = event[1][0][1].overflowed

            event[1][0][1].put_trash_in(event[1][0][3])
            if not show_stats_only:
                print(
                    str_to_print + " -> " + "bag weighing " + str(
                        round(event[1][0][3].bag_weight, 3)) + " kg disposed of at bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id))
                if overflowed:
                    pass

                elif (not exceeded and not event[1][0][1].exceeded) or (
                        exceeded and not overflowed and not event[1][0][1].overflowed):
                    print(str_to_print + " -> " + "load of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " became " + str(
                        round(event[1][0][1].current_weight, 3)) + " kg and contents volume " + str(
                        round(event[1][0][1].current_volume, 3)) + " m^3")

                elif not exceeded and event[1][0][1].exceeded and not event[1][0][1].overflowed:
                    print(str_to_print + " -> " + "load of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " became " + str(
                        round(event[1][0][1].current_weight, 3)) + " kg and contents volume " + str(
                        round(event[1][0][1].current_volume, 3)) + " m^3")
                    print(str_to_print + " -> " + "occupancy threshold of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " exceeded")
                elif exceeded and not overflowed and event[1][0][1].overflowed:
                    print(str_to_print + " -> " + "load of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " became " + str(
                        round(event[1][0][1].current_weight, 3)) + " kg and contents volume " + str(
                        round(event[1][0][1].current_volume, 3)) + " m^3")
                    print(
                        str_to_print + " -> " + "bin " + str(
                            event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " overflowed")


                elif not exceeded and event[1][0][1].overflowed:
                    print(str_to_print + " -> " + "load of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " became " + str(
                        round(event[1][0][1].current_weight, 3)) + " kg and contents volume " + str(
                        round(event[1][0][1].current_volume, 3)) + " m^3")
                    print(str_to_print + " -> " + "occupancy threshold of bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " exceeded")
                    print(str_to_print + " -> " + "bin " + str(
                        event[1][0][0]) + "." + str(event[1][0][1].bin_id) + " overflowed")

        if event_description == 'dispose':

            for area in bins_areas:

                if event[1][0][0] == area['areaIdx']:

                    for bin_id, bin in enumerate(area['roadsLayout'].nodes(data=True)[1:], start=1):

                        bin = bin[1]['bin']
                        if bin == event[1][0][1]:
                            delay = 0
                            for i in range(0, model['disposalDistrShape'][0]):
                                delay += -(1 / model['disposalDistrRate'][0]) * np.log(np.random.random())

                            time_future = time + timedelta(hours=delay)
                            bag = TrashBag(model['bagVolume'], model['bagWeightMin'], model['bagWeightMax'])
                            bin_fill_queue.put(
                                (time_future, [(area['areaIdx'], bin, 'dispose', bag)]))

    print('---')

    if model['warmUpTime'] >= model['stopTime']:
        print('Sorry! Statistics could not be output because warmUpTime >= stopTime.')
        return

    # Stats collection
    average_overall = []
    for ind_area,atd in enumerate(average_trip_duration):
        avg_time = average_timedelta(average_trip_duration[ind_area])
        time_print = pretty_time_delta(avg_time)
        print('area ' + str(ind_area) + ":" + " average trip duration " + time_print)
        average_overall.append(avg_time)

    overall_time_duration = average_timedelta(average_overall)
    time_print_overall = pretty_time_delta(overall_time_duration)
    print('overall average trip duration ' + time_print_overall)

    average_overall_trips = []
    for ind_area2, trips_no in enumerate(no_trips_areas):
        num_schedules = (model['stopTime'] - model['warmUpTime'])*bins_areas[ind_area2]['serviceFreq'][0]
        average_trips_area = no_trips_areas[ind_area2]/num_schedules
        print('area ' + str(ind_area2) + ":" + " average no. trips " + str(round(average_trips_area,3)))
        average_overall_trips.append(average_trips_area)

    overall_num_trips = sum(average_overall_trips)/len(average_overall_trips)
    print('overall no. trips ' + str(round(overall_num_trips,3)))

    average_overall_trip_efficiency = []

    for ind_area3, area_trips in enumerate(average_trip_efficiency):
        area_efficiency = get_area_trip_efficiency(average_trip_efficiency[ind_area3])
        print('area ' + str(ind_area3) + ": trip efficiency " + str(round(area_efficiency,3)))
        average_overall_trip_efficiency.append(area_efficiency)


    overall_trip_efficiency = sum(average_overall_trip_efficiency)/len(average_overall_trip_efficiency)
    print('overall trip efficiency ' + str(round(overall_trip_efficiency,3)))

    average_overall_volume_collected = []

    for ind_area4, area_trip_vol in enumerate(average_volume_collected):
        area_volume = get_area_trip_efficiency(average_volume_collected[ind_area4])
        print('area ' + str(ind_area4) + ": average volume collected " + str(round(area_volume,3)))
        average_overall_volume_collected.append(area_volume)


    average_volume = sum(average_overall_volume_collected)/len(average_overall_volume_collected)
    print('overall average volume collected ' + str(round(average_volume,3)))

    average_percentages_overflowed = []

    for ind_area5, area_percentages in enumerate(average_percentage_overflowed):
        if len(average_percentage_overflowed[ind_area5])>0:
            avg_area_percent = sum(average_percentage_overflowed[ind_area5])/len(average_percentage_overflowed[ind_area5])
        else:
            avg_area_percent = 0
        print('area ' + str(ind_area5) + ": percentage of bins overflowed " + str(round(avg_area_percent,3)))
        average_percentages_overflowed.append(avg_area_percent)

    avg_overall_percent_overflowed = sum(average_percentages_overflowed)/len(average_percentages_overflowed)
    print('overall percentage of bins overflowed ' + str(round(avg_overall_percent_overflowed,3)))

    print('---')

def lorry_return(areaGraph):
    """
    Return the reference for the lorry in the specified areaGraph.

    :param: areaGraph:
    :return: Lorry reference.
    """
    for node in areaGraph.nodes(data=True):
        if 'lorry' in node[1].keys():
            return node[1]['lorry']


def get_area_trip_efficiency(area):
    area_trip_efficiencies = []
    for trip_tuple_weight in area:
        if type(trip_tuple_weight) == tuple:
            mins_trip = convert_td_minutes(trip_tuple_weight[0])
            area_trip_efficiencies.append(trip_tuple_weight[1]/mins_trip)
    efficiency = sum(area_trip_efficiencies)/len(area_trip_efficiencies)
    return efficiency



def convert_td_minutes(duration):

    """
    Convert timedelta to minutes

    :param duration: duration of trip
    :return: converted duration to minutes
    """

    total_seconds = duration.total_seconds()
    minutes = total_seconds/60
    return minutes

def route_plan(time, priority_queue, area, model, collected_all, bins_left):

    """
    Performs route planning, as explained in the report, puts events in the queue for the relevant lorry.

    :param time_trip: The time when the lorry has arrived to the depot.
    :param area: a dictionary containing the road layout, among other things
    :param priority_queue: the queue for the relevant lorry
    :param model: the dictionary of simple attributes
    :param collected_all: True if lorry returned to depot because it has collected all the trash - false if not.
    :param bins_left: The bins that are yet to be emptied
    :return: bins_to_collect - dictionary of the form {'highestPriority':[bin1, bin2, etc], 'mediumPriority':[bin19, bin20, etc.], 'lowestPriority':['bin43, bin74 etc']}
    """

    if not collected_all:
        prioritized_bins = bins_left
    else:
        prioritized_bins = prioritize_bins(area, model)

    prioritized_bins_copy = copy.deepcopy(prioritized_bins)
    time_trip = time
    path_to_follow = []
    index_shortest = 0
    emptying_order = []

    if len(prioritized_bins['highestPriority'])>0:
        path_to_node = nx.shortest_path(area['roadsLayout'],0,prioritized_bins['highestPriority'][0], weight='weight')
        shortest = nx.shortest_path_length(area['roadsLayout'], 0 ,prioritized_bins['highestPriority'][0], weight='weight')
        idx_to_remove = prioritized_bins['highestPriority'][0]
        if len(prioritized_bins['highestPriority'])>1:
            for overflowed in prioritized_bins['highestPriority'][1:]:
                path = nx.shortest_path(area['roadsLayout'], 0, overflowed, weight='weight')
                distance = nx.shortest_path_length(area['roadsLayout'], 0, overflowed, weight='weight')
                if distance < shortest:
                    shortest = distance
                    idx_to_remove = overflowed
                    path_to_node = path
        index_shortest = idx_to_remove
        emptying_order.append((index_shortest,'highest'))

        path_to_follow.append(path_to_node)

        prioritized_bins['highestPriority'].remove(index_shortest)


        while not prioritized_bins['highestPriority'] == []:

            path_to_node = nx.shortest_path(area['roadsLayout'], index_shortest, prioritized_bins['highestPriority'][0], weight='weight')
            shortest = nx.shortest_path_length(area['roadsLayout'], index_shortest, prioritized_bins['highestPriority'][0], weight='weight')
            idx_to_remove = prioritized_bins['highestPriority'][0]

            if len(prioritized_bins['highestPriority']) > 1:
                for overflowed in prioritized_bins['highestPriority'][1:]:
                    path = nx.shortest_path(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    distance = nx.shortest_path_length(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    if distance < shortest:
                        shortest = distance
                        idx_to_remove = overflowed
                        path_to_node = path

            index_shortest = idx_to_remove
            path_to_follow.append(path_to_node)
            prioritized_bins['highestPriority'].remove(index_shortest)
            emptying_order.append((index_shortest, 'highest'))

    if len(prioritized_bins['mediumPriority'])>0:

        path_to_node = nx.shortest_path(area['roadsLayout'],index_shortest,prioritized_bins['mediumPriority'][0], weight='weight')
        shortest = nx.shortest_path_length(area['roadsLayout'],index_shortest,prioritized_bins['mediumPriority'][0], weight='weight')
        idx_to_remove = prioritized_bins['mediumPriority'][0]

        if len(prioritized_bins['mediumPriority'])>1:
            for overflowed in prioritized_bins['mediumPriority'][1:]:
                path = nx.shortest_path(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                distance = nx.shortest_path_length(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                if distance < shortest:
                    shortest = distance
                    idx_to_remove = overflowed
                    path_to_node = path

        index_shortest = idx_to_remove
        path_to_follow.append(path_to_node)
        prioritized_bins['mediumPriority'].remove(index_shortest)
        emptying_order.append((index_shortest, 'medium'))

        while not prioritized_bins['mediumPriority'] == []:

            path_to_node = nx.shortest_path(area['roadsLayout'], index_shortest, prioritized_bins['mediumPriority'][0], weight='weight')
            shortest = nx.shortest_path_length(area['roadsLayout'], index_shortest, prioritized_bins['mediumPriority'][0], weight='weight')
            idx_to_remove = prioritized_bins['mediumPriority'][0]

            if len(prioritized_bins['mediumPriority']) > 1:
                for overflowed in prioritized_bins['mediumPriority'][1:]:
                    path = nx.shortest_path(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    distance = nx.shortest_path_length(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    if distance < shortest:
                        shortest = distance
                        idx_to_remove = overflowed
                        path_to_node = path

            index_shortest = idx_to_remove
            path_to_follow.append(path_to_node)
            prioritized_bins['mediumPriority'].remove(index_shortest)
            emptying_order.append((index_shortest, 'medium'))

    if len(prioritized_bins['lowestPriority']) > 0:

        path_to_node = nx.shortest_path(area['roadsLayout'], index_shortest, prioritized_bins['lowestPriority'][0], weight='weight')
        shortest = nx.shortest_path_length(area['roadsLayout'], index_shortest, prioritized_bins['lowestPriority'][0], weight='weight')
        idx_to_remove = prioritized_bins['lowestPriority'][0]

        if len(prioritized_bins['lowestPriority']) > 1:
            for overflowed in prioritized_bins['lowestPriority'][1:]:
                path = nx.shortest_path(area['roadsLayout'], index_shortest, overflowed,weight='weight')
                distance = nx.shortest_path_length(area['roadsLayout'], index_shortest, overflowed,weight='weight')
                if distance < shortest:
                    shortest = distance
                    idx_to_remove = overflowed
                    path_to_node = path

        index_shortest = idx_to_remove
        path_to_follow.append(path_to_node)
        prioritized_bins['lowestPriority'].remove(index_shortest)
        emptying_order.append((index_shortest, 'lowest'))

        while not prioritized_bins['lowestPriority'] == []:

            path_to_node = nx.shortest_path(area['roadsLayout'], index_shortest, prioritized_bins['lowestPriority'][0], weight='weight')
            shortest = nx.shortest_path_length(area['roadsLayout'], index_shortest, prioritized_bins['lowestPriority'][0], weight='weight')
            idx_to_remove = prioritized_bins['lowestPriority'][0]

            if len(prioritized_bins['lowestPriority']) > 1:
                for overflowed in prioritized_bins['lowestPriority'][1:]:
                    path = nx.shortest_path(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    distance = nx.shortest_path_length(area['roadsLayout'], index_shortest, overflowed, weight='weight')
                    if distance < shortest:
                        shortest = distance
                        idx_to_remove = overflowed
                        path_to_node = path

            index_shortest = idx_to_remove
            path_to_follow.append(path_to_node)
            prioritized_bins['lowestPriority'].remove(index_shortest)
            emptying_order.append((index_shortest, 'lowest'))

    flattened_path = sum(path_to_follow,[])

    for i, el in enumerate(flattened_path[:-1]):

        if flattened_path[i + 1] == flattened_path[i]:
            continue
        path_len = nx.shortest_path_length(area['roadsLayout'], flattened_path[i], flattened_path[i+1], weight='weight')
        priority_queue.put((time_trip, [(area['areaIdx'], flattened_path[i], 'leaving')]))
        time_trip += timedelta(minutes=path_len)
        priority_queue.put((time_trip, [(area['areaIdx'], flattened_path[i+1], 'arriving')]))

        if flattened_path[i+1] == emptying_order[0][0]:

            emptying_order.pop(0)

            time_trip += timedelta(seconds=model['binServiceTime'])
            priority_queue.put((time_trip,[(area['areaIdx'], flattened_path[i+1], 'emptying')]))

    return prioritized_bins_copy

def return_to_depot(time_trip, current_location, area, priority_queue, model, collected_all, bins_left):

    """
    Finds the shortest path to the depot.

    :param time_trip: The time when the lorry decides to go back to the depot.
    :param current_location: The current location of the lorry.
    :param area: a dictionary containing the road layout, among other things
    :param priority_queue: the queue for the relevant lorry
    :param model: the dictionary of simple attributes
    :param collected_all: True if lorry returns because it has collected all the trash - false if not.
    :param bins_left: The bins that are yet to be emptied

    """

    path_to_depot = nx.shortest_path(area['roadsLayout'], current_location, 0, weight='weight')

    # Put events in the queue for leaving and arriving at intermediate stops to the depot, advancing the time.
    for i,_ in enumerate(path_to_depot[:-1]):
        if path_to_depot[i + 1] == path_to_depot[i]:
            continue

        path_len = nx.shortest_path_length(area['roadsLayout'], path_to_depot[i], path_to_depot[i+1], weight='weight')

        priority_queue.put((time_trip, [(area['areaIdx'], path_to_depot[i], 'leaving')]))
        time_trip += timedelta(minutes=path_len)
        priority_queue.put((time_trip, [(area['areaIdx'], path_to_depot[i + 1], 'arriving')]))

    # Advance the time with the time it takes to empty a lorry.
    time_trip += timedelta(seconds = model['binServiceTime']*5)
    priority_queue.put((time_trip,[(area['areaIdx'], 0, 'emptyLorry')]))

    # If not all bins have been collected, plan route for the rest.
    if not collected_all:
        route_plan(time_trip, priority_queue, area, model, False, bins_left)

def prioritize_bins(area, model):

    """
    Prioritize the bins to visit in an area.
    """
    prioritized_bins = {'highestPriority':[],'mediumPriority':[], 'lowestPriority':[]}
    G = area['roadsLayout']

    for node in G.nodes(data=True)[1:]:

        if node[1]['bin'].current_weight >= model['lorryMaxLoad'] or node[1]['bin'].current_volume >= model['lorryVolume']:    # if the current weight or volume of a bin is larger than the lorry constraints - don't bother visiting it.
            continue
        if node[1]['bin'].overflowed:
            prioritized_bins['highestPriority'].append(node[0])

        elif node[1]['bin'].exceeded and not node[1]['bin'].overflowed:
            prioritized_bins['mediumPriority'].append(node[0])
        else:
            prioritized_bins['lowestPriority'].append(node[0])
    return prioritized_bins


def all_PQs_empty(list_PQs):

    """
    Returns whether (bool) all priority queues in a list are empty.
    """

    for pq in list_PQs:
        if not pq.empty():
            return False
    return True

def count_percentage_overflowed(area):

    """
    Count the percentage of overflowed bins in the area "area"(the function argument).

    :param area: the area description
    :return: The percentage
    """

    G = area['roadsLayout']
    overflowed = 0
    number_bins = len(G.nodes(data=True)[1:])
    for node in G.nodes(data=True)[1:]:
        if node[1]['bin'].overflowed:
            overflowed += 1

    return (overflowed/number_bins)*100

def algorithm_exp(model, area_descriptions):

    """
    Runs the function algorithm() several times with appropriate parameters, disabling detailed output and showing summary statistics only.

    :param model:
    :param area_descriptions:

    """

    if model['warmUpTime'] >= model['stopTime']:
        print('Error! Statistics cannot be collected cannot be collected because warmUpTime >= stopTime.')
        return


    service_freq_experimented = len(area_descriptions[0]['serviceFreq']) > 1

    exp_count = 1

    if not service_freq_experimented:

        temp_area_descriptions = copy.deepcopy(area_descriptions)
        for rate in model['disposalDistrRate']:
            for shape in model['disposalDistrShape']:
                new_model = copy.deepcopy(model)
                new_model['disposalDistrRate'] = [rate]
                new_model['disposalDistrShape'] = [shape]
                print(
                "Experiment #" + str(exp_count) + ": disposalDistrRate " + str(rate) + " disposalDistrShape " + str(
                    shape))
                algorithm(new_model, temp_area_descriptions, 1)
                temp_area_descriptions = copy.deepcopy(area_descriptions)
                exp_count+=1
    else:
        freqs = area_descriptions[0]['serviceFreq']
        temp_area_descriptions = copy.deepcopy(area_descriptions)
        for rate in model['disposalDistrRate']:
            for shape in model['disposalDistrShape']:
                for freq in freqs:
                    new_model = copy.deepcopy(model)
                    new_model['disposalDistrRate'] = [rate]
                    new_model['disposalDistrShape'] = [shape]
                    for a in temp_area_descriptions:
                        a['serviceFreq'] = [freq]

                    print(
                    "Experiment #" + str(exp_count) + ": disposalDistrRate " + str(rate) + " disposalDistrShape " + str(
                        shape) + " serviceFreq " + str(freq))
                    algorithm(new_model, temp_area_descriptions, 1)
                    temp_area_descriptions = copy.deepcopy(area_descriptions)
                    exp_count += 1

def average_timedelta(delta_list):
    return sum(delta_list, timedelta()) // len(delta_list)

# https://gist.github.com/thatalextaylor/7408395

def pretty_time_delta(duration):
    """Converts timedelta to days:hours:minutes:seconds format."""
    seconds = duration.total_seconds()
    seconds = int(seconds)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%d:%d:%d:%d' % (days, hours, minutes, seconds)
    elif hours > 0:
        return '%d:%d:%d' % (hours, minutes, seconds)
    elif minutes > 0:
        return '%d:%d' % (minutes, seconds)
    else:
        return '%d' % (seconds,)

if __name__ == "__main__":
    main()